# Gravity Forms Manual Payments Add-On

Adds a form setting allowing the editing of payment details on the entry detail page to be enabled for forms not processed by other payment add-ons.

Requires Gravity Forms 2.0.

## Why would you want to do this?

If you allow people to pay by check or other non-electronic means, you might want to manually update their entries to show that they had been paid (the check was received), refuded, or cancelled. This plugin allows you to do that.

It also calls sends the appropriate notifications and calls the appropriate hooks so that these manual payments are treated in the same way that electronic payments made through payment add-ons are.

## Installation

 1. Download ZIP and install via WordPress admin or upload plugin to the /wp-content/plugins/ directory
 2. Activate the plugin through the 'Plugins' menu in the WordPress Admin Panel
 3. Enable the setting for your form by going to the Form Settings > Manual Payments area in the form editor
 
## History
 
This add-on was created by [Eric Celeste](http://eric.clst.org) of [Tenseg LLC](http://tenseg.net) in April 2018.
 
Thank you to Richard Wawrzyniak who created the simpler [Enable Gravity Forms Payment Details](https://github.com/richardW8k/enablegfpaymentdetails) that inspired and forms the foundation for this add-on.