<?php
GFForms::include_addon_framework();

/**
 * GF_Manual_Payments Class
 *
 * A Gravity Forms Add On to facilitate the manual
 * resolution of payments that are not using another
 * payments Add On.
 */
class GF_Manual_Payments extends GFAddOn {

	/**
	 * The version of this add-on
	 *
	 * @var string
	 * @access protected
	 */
	protected $_version = '0.1';

	/**
	 * The version of Gravity Forms required for this add-on
	 *
	 * @var string
	 * @access protected
	 */
	protected $_min_gravityforms_version = '2.0';

	/**
	 * A short, lowercase, URL-safe unique identifier for the add-on
	 *
	 * @var string
	 * @access protected
	 */
	protected $_slug = 'gfmanualpayments';

	/**
	 * Relative path to the plugin from the plugins folder
	 *
	 * @var string
	 * @access protected
	 */
	protected $_path = 'gravityformsmanualpayments/gravityformsmanualpayments.php';

	/**
	 * The physical path to the main plugin file
	 *
	 * @var string
	 * @access protected
	 */
	protected $_full_path = __FILE__;

	/**
	 * The complete title of the Add-On
	 *
	 * @var string
	 * @access protected
	 */
	protected $_title = 'Manual Payments Add-On';

	/**
	 * The short title of the Add-On to be used in limited spaces
	 *
	 * @var string
	 * @access protected
	 */
	protected $_short_title = 'Manual Payments';

	/**
	 * Capabilities or roles that have access to the form settings.
	 *
	 * @var string|array a string or an array of capabilities
	 */
	protected $_capabilities_form_settings = 'gfmanualpayments_settings';

	/**
	 * Capabilities or roles that can uninstall the plugin.
	 *
	 * @var string|array a string or an array of capabilities
	 */
	protected $_capabilities_uninstall = 'gfmanualpayments_uninstall';

	/**
	 * List of capabilities to add to roles.
	 *
	 * @var array capabilities to add to roles.
	 */
	protected $_capabilities = ['gfmanualpayments_settings', 'gfmanualpayments_uninstall'];

	/**
	 * An instance of this class
	 *
	 * (default value: null)
	 *
	 * @var mixed
	 * @access private
	 * @static
	 */
	private static $_instance = null;

	/**
	 * Get an instance of this class.
	 *
	 * @return GF_Manual_Payments
	 */
	public static function get_instance() {
		if ( null == self::$_instance ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Perform tasks that need to be done after the WordPress action "init" fires
	 *
	 * @access public
	 * @return void
	 */
	public function init() {
		parent::init();
		if ( $this->is_gravityforms_supported() ) {
			// after submission has an earlier execution priority to ensure processing before paid
			add_action( 'gform_after_submission', [$this, 'set_processing_status'], 5, 2 );
		}
	}

	/**
	 * Include the hooks.
	 */
	public function init_admin() {
		parent::init_admin();
		if ( $this->is_gravityforms_supported() ) {
			add_filter( 'gform_entry_detail_meta_boxes', [$this, 'add_payment_details_meta_box'], 10, 3 );

			//add actions to allow the payment status to be modified
			add_action( 'gform_payment_status', [$this, 'admin_edit_payment_status'], 10, 3 );
			add_action( 'gform_payment_date', [$this, 'admin_edit_payment_date'], 10, 3 );
			add_action( 'gform_payment_transaction_id', [$this, 'admin_edit_payment_transaction_id'], 10, 3 );
			add_action( 'gform_payment_amount', [$this, 'admin_edit_payment_amount'], 10, 3 );
			add_action( 'gform_after_update_entry', [$this, 'set_payment_status'], 10, 3 );
			add_filter( 'gform_notification_events', [$this, 'notification_events'], 10, 2 );
		}
	}

	/**
	 * Returns the current form object based on the id query var. Otherwise returns false.
	 *
	 * @return array|boolean a GF Form object
	 */
	public function get_current_form() {
		return $this->is_entry_view() || $this->is_entry_edit() ? GFEntryDetail::get_current_form() : parent::get_current_form();
	}

	/**
	 * Return true if manual payments are enabled on this form.
	 *
	 * @param array|boolean $form
	 * @return boolean
	 */
	public function manual_payments_enabled_for_form( $form = false ) {
		if ( empty( $form ) ) {
			$form = $this->get_current_form();
		}

		// check form settings
		$settings = $this->get_form_settings( $form );
		$field = rgar( $settings, 'manual_payments_method_field' );
		$value = rgar( $settings, 'manual_payments_method_value' );

		// check for existence of payment method field
		// since if it isn't there manual payments are
		// inherently not enabled regardless of the settings
		foreach ( $form['fields'] as $field_object ) {
			if ( $field_object->inputName == $field ) {
				$payment_options_field_found = true;
			}
		}

		// return a response using the above checks
		return $field && $value && isset( $payment_options_field_found );
	}

	/**
	 * Return true if manual payments are enabled on this entry.
	 *
	 * @param array $entry an Entry object
	 * @return boolean
	 */
	public function manual_payments_enabled_for_entry( $entry ) {
		if ( !( $settings = $this->get_form_settings( GFAPI::get_form( intval(  ( $entry['form_id'] ) ) ) ) ) ) {
			return false;
		}

		$field = rgar( $settings, 'manual_payments_method_field' );
		$value = rgar( $settings, 'manual_payments_method_value' );

		return $field && $value && TGMemGF::field_value( $field, $entry ) == $value;
	}

	/**
	 * Specify the settings fields to be rendered on the form settings page.
	 *
	 * @param array a GF Form object
	 */
	public function form_settings_fields( $form ) {
		return [
			[
				'title'       => $this->get_short_title(),
				'description' => 'If this form does not have any payments <strong>please ensure these fields are left blank.</strong>',
				'fields'      => [
					[
						'name'    => 'manual_payments_method_field',
						'label'   => esc_html__( 'Field Name for Manual Payments Method', 'tgmem' ),
						'tooltip' => esc_html__( 'This is the name of the method field, often simply "method".', 'tgmem' ),
						'type'    => 'text',
						'class'   => 'medium',
					],
					[
						'name'    => 'manual_payments_method_value',
						'label'   => esc_html__( 'Value for Manual Payments Method', 'tgmem' ),
						'tooltip' => esc_html__( 'This is the value that indicates manual payments should be allowed, often simply "check".', 'tgmem' ),
						'type'    => 'text',
						'class'   => 'medium',
					],
				],
			],
		];
	}

	/**
	 * Add a payment details meta box to the entry details.
	 *
	 * Called by: gform_entry_detail_meta_boxes
	 *
	 * This filter can be used to add custom meta boxes to the entry detail page.
	 *
	 * @param array $meta_boxes properties for the meta boxes
	 * @param array $entry the GF Entry object currently being viewed/edited
	 * @param array $form the GF Form object used to process the current entry
	 * @return array revised properties for the meta boxes
	 */
	public function add_payment_details_meta_box( $meta_boxes, $entry, $form ) {
		if ( !isset( $meta_boxes['payment'] ) && $this->manual_payments_enabled_for_form( $form ) ) {
			GFAPI::update_entry_property( $entry['id'], 'payment_status', 'Processing' );
			GFAPI::update_entry_property( $entry['id'], 'transaction_type', '1' );
			$entry['payment_status'] = 'Processing';
			$entry['transaction_type'] = '1';
			GFEntryDetail::set_current_entry( $entry );

			$meta_boxes['payment'] = [
				'title'    => esc_html__( 'Payment Details', 'gravityforms' ),
				'callback' => ['GFEntryDetail', 'meta_box_payment_details'],
				'context'  => 'side',
			];
		}

		return $meta_boxes;
	}

	/**
	 * Return elements of the form for modifying the payment status if editing is enabled.
	 *
	 * Called by: gform_payment_status
	 *
	 * Filter allows for modification of the payment status in the payment meta box.
	 *
	 * @param string $payment_status the payment status to filter
	 * @param array $form the GF Form object used to process the current entry
	 * @param array $entry the GF Entry object currently being viewed/edited
	 * @return string the HTML for what we want the payment status to look like
	 */
	public function admin_edit_payment_status( $payment_status, $form, $entry ) {
		if ( $this->payment_details_editing_disabled( $entry ) ) {
			return $payment_status;
		}

		//create drop down for payment status
		$payment_string = '<select id="payment_status" name="payment_status">';
		$payment_string .= '<option value="' . $payment_status . '" selected>' . $payment_status . '</option>';
		switch ( $payment_status ) {
			case 'Processing':
				$payment_string .= '<option value="Paid">Paid</option>';
				$payment_string .= '<option value="Failed">Failed</option>';
				break;
			case 'Paid':
				$payment_string .= '<option value="Refunded">Refunded</option>';
				break;
		}
		$payment_string .= '</select>';

		return $payment_string;
	}

	/**
	 * Return elements of the form for modifying the payment date if editing is enabled.
	 *
	 * Called by: gform_payment_date
	 *
	 * Filter allows for modification of the payment date in the payment meta box.
	 *
	 * @param string $payment_date the payment date to filter
	 * @param array $form the GF Form object used to process the current entry
	 * @param array $entry the GF Entry object currently being viewed/edited
	 * @return string the HTML for what we want the payment date to look like
	 */
	public function admin_edit_payment_date( $payment_date, $form, $entry ) {
		if ( $this->payment_details_editing_disabled( $entry ) ) {
			return $payment_date;
		}

		$payment_date = $entry['payment_date'];
		if ( empty( $payment_date ) ) {
			$payment_date = gmdate( 'y-m-d H:i:s' );
		}

		$input = '<input type="text" id="payment_date" name="payment_date" value="' . $payment_date . '">';

		return $input;
	}

	/**
	 * Return elements of the form for modifying the transaction ID if editing is enabled.
	 *
	 * Called by: gform_payment_transaction_id
	 *
	 * Filter allows for modification of the transaction ID in the payment meta box.
	 *
	 * @param string $transaction_id the transaction ID to filter
	 * @param array $form the GF Form object used to process the current entry
	 * @param array $entry the GF Entry object currently being viewed/edited
	 * @return string the HTML for what we want the transaction ID to look like
	 */
	public function admin_edit_payment_transaction_id( $transaction_id, $form, $entry ) {
		if ( $this->payment_details_editing_disabled( $entry ) ) {
			return $transaction_id;
		}

		$input = '<input type="text" id="custom_transaction_id" name="custom_transaction_id" value="' . $transaction_id . '">';

		return $input;
	}

	/**
	 * Return elements of the form for modifying the payment amount if editing is enabled.
	 *
	 * Called by: gform_payment_amount
	 *
	 * Filter allows for modification of the transaction ID in the payment meta box.
	 *
	 * @param string $payment_amount the payment amount to filter
	 * @param array $form the GF Form object used to process the current entry
	 * @param array $entry the GF Entry object currently being viewed/edited
	 * @return string the HTML for what we want the payment amount to look like
	 */
	public function admin_edit_payment_amount( $payment_amount, $form, $entry ) {
		if ( $this->payment_details_editing_disabled( $entry ) ) {
			return $payment_amount;
		}

		if ( empty( $payment_amount ) ) {
			$payment_amount = GFCommon::get_order_total( $form, $entry );
		}

		$payment_amount = GFCommon::to_money( $payment_amount, $entry['currency'] );

		$input = '<input type="text" id="payment_amount" name="payment_amount" class="gform_currency" value="' . $payment_amount . '">';

		return $input;
	}

	/**
	 * Make sure a payment_status of 'Processing' is recorded.
	 *
	 * Called by: gform_after_submission
	 *
	 * This action is executed at the end of the submission process
	 * (after form validation, notification, and entry creation).
	 * Use this hook to perform actions after the entry has been
	 * created (i.e. feed data to third party applications).
	 *
	 * @param array $entry the GF Entry Object that has been submitted
	 * @param array $form the GF Form Object from which the entry came
	 * @return void
	 */
	public function set_processing_status( $entry, $form ) {

		// if the method was "check" and has an outstanding amount due
		// then the entry should be given a "Processing" status,
		if ( !$this->manual_payments_enabled_for_entry( $entry ) ) {
			return;
		}

		// ignore entries that have a payment status of any sort
		if ( null !== $entry['payment_status'] ) {
			return;
		}

		// ignore entries with any kind of payment method
		if ( null !== $entry['payment_method'] ) {
			return;
		}

		// update the stored entry properties
		GFAPI::update_entry_property( $entry['id'], 'payment_status', 'Processing' );
	}

	/**
	 * Launch notification and actions related to the changed payments.
	 *
	 * Called by: gform_after_update_entry
	 *
	 * This action fires after the entry has been updated via the entry detail page.
	 *
	 * @param array $form the GF Form object used to process the current entry
	 * @param integer $entry_id the ID of the GF Entry object currently being viewed/edited
	 * @param array $original_entry the GF Entry object as it was before being updated
	 * @return void
	 */
	public function set_payment_status( $form, $entry_id, $original_entry ) {
		check_admin_referer( 'gforms_save_entry', 'gforms_save_entry' );

		// update payment information in admin,
		// need to use this function so the lead data
		// is updated before displayed in the sidebar info section
		$entry = GFFormsModel::get_lead( $entry_id );

		if ( !$this->manual_payments_enabled_for_entry( $entry ) ) {
			return;
		}

		if ( $this->payment_details_editing_disabled( $entry, 'update' ) ) {
			return;
		}

		// get payment fields to update
		$payment_status = rgpost( 'payment_status' );

		// when updating, payment status may not be editable, if no value in post, set to lead payment status
		if ( empty( $payment_status ) ) {
			$payment_status = $entry['payment_status'];
		}

		// only process changes in payment status, ignore any status that stays the same
		if ( $payment_status === $original_entry['payment_status'] ) {
			return;
		}

		// only handle these three transactions
		$payment_types = [
			'Paid'     => 'complete_payment',
			'Refunded' => 'refund_payment',
			'Failed'   => 'fail_payment',
		];

		if ( !( $payment_type = rgar( $payment_types, $payment_status ) ) ) {
			return;
		}

		$transaction_types = [
			'Paid'     => 'payment',
			'Refunded' => 'refund',
			'Failed'   => 'cancellation',
		];

		$notes = [
			'Paid'     => 'manually completed',
			'Refunded' => 'manually refunded',
			'Failed'   => 'manually cancelled',
		];

		$payment_method = 'manual';
		$payment_amount = GFCommon::to_number( rgpost( 'payment_amount' ), $entry['currency'] );
		$payment_transaction = rgpost( 'custom_transaction_id' );
		$payment_date = rgpost( 'payment_date' );
		if ( empty( $payment_date ) ) {
			$payment_date = gmdate( 'y-m-d H:i:s' );
		} else {
			// format date entered by user
			$payment_date = date( 'Y-m-d H:i:s', strtotime( $payment_date ) );
		}

		$current_user = wp_get_current_user();

		// update the action we will share
		$action = [
			'type'             => $payment_type,
			'amount'           => $payment_amount,
			'transaction_type' => rgar( $transaction_types, $payment_status ),
			'transaction_id'   => $payment_transaction,
			'subscription_id'  => '',
			'entry_id'         => $entry_id,
			'payment_status'   => $payment_status,
			'note'             => sprintf( __( 'Payment %s by %s. Status: %s. Amount: %s. Transaction Id: %s. Date: %s', $this->_slug ), rgar( $notes, $payment_status ), $current_user->user_login, $payment_status, GFCommon::to_money( $payment_amount, $entry['currency'] ), $payment_transaction, $payment_date ),
		];

		// update the stored entry properties
		GFAPI::update_entry_property( $entry['id'], 'payment_status', $payment_status );
		GFAPI::update_entry_property( $entry['id'], 'payment_amount', $payment_amount );
		GFAPI::update_entry_property( $entry['id'], 'payment_date', $payment_date );
		GFAPI::update_entry_property( $entry['id'], 'transaction_id', $payment_transaction );
		GFAPI::update_entry_property( $entry['id'], 'payment_method', $payment_method );

		// also update our current entry properties
		$entry['payment_status'] = $payment_status;
		$entry['payment_amount'] = $payment_amount;
		$entry['payment_date'] = $payment_date;
		$entry['transaction_id'] = $payment_transaction;
		$entry['payment_method'] = $payment_method;

		// add a note
		$this->add_note( $entry['id'], $action['note'] );

		GFAPI::send_notifications( $form, $entry, $payment_types[$payment_status] );

		switch ( $payment_status ) {
			case 'Paid':
				do_action( 'gform_post_payment_completed', $entry, $action );
				if ( has_filter( 'gform_post_payment_completed' ) ) {
					$this->log_debug( __METHOD__ . '(): Executing functions hooked to gform_post_payment_completed.' );
				}
				break;
			case 'Refunded':
				do_action( 'gform_post_payment_refunded', $entry, $action );
				if ( has_filter( 'gform_post_payment_refunded' ) ) {
					$this->log_debug( __METHOD__ . '(): Executing functions hooked to gform_post_payment_refunded.' );
				}
				break;
			case 'Failed':
				do_action( 'gform_post_payment_failed', $entry, $action );
				if ( has_filter( 'gform_post_payment_failed' ) ) {
					$this->log_debug( __METHOD__ . '(): Executing functions hooked to gform_post_payment_failed.' );
				}
				break;
		}
	}

	/**
	 * Return true if we should not try to edit the payment details.
	 *
	 * This tries to sense if a true payment gateway like Stripe or Paypal is
	 * responsible for this entry, and if so, then disallow editing.
	 *
	 * @param array $entry the GF Entry object currently being viewed/edited
	 * @param string $action passes along the indended action
	 * @return boolean true if we should not be allowed to edit
	 */
	public function payment_details_editing_disabled( $entry, $action = 'edit' ) {
		if ( !$this->manual_payments_enabled_for_form() ) {
			return true;
		}

		$gateway = gform_get_meta( $entry['id'], 'payment_gateway' );
		if ( $gateway
			|| !( rgar( $entry, 'payment_status' ) === 'Processing' || rgar( $entry, 'payment_status' ) === 'Paid' ) ) {
			// Entry was processed by a payment add-on, don't allow editing.
			return true;
		}

		if ( 'edit' == $action && rgpost( 'screen_mode' ) == 'edit' ) {
			// Editing is allowed for this entry.
			return false;
		}

		if ( 'update' == $action && rgpost( 'screen_mode' ) == 'view' && rgpost( 'action' ) == 'update' ) {
			// Updating the payment details for this entry is allowed.
			return false;
		}

		// In all other cases editing is not allowed.

		return true;
	}

	/**
	 * Add notifications events supported by Add-On to notification events list.
	 *
	 * Called by: gform_notification_events
	 *
	 * Filter to add a new event to the drop down of available triggers for notifications.
	 *
	 * @param array $events the array of notification events
	 * @param array $form the GF Form object used to process the current entry
	 * @return array $events the modified array of notification events
	 */
	public function notification_events( $events, $form ) {
		if ( !isset( $events['complete_payment'] ) ) {
			$events['complete_payment'] = esc_html__( 'Payment Completed', $this->_slug );
		}
		if ( !isset( $events['refund_payment'] ) ) {
			$events['refund_payment'] = esc_html__( 'Payment Refunded', $this->_slug );
		}
		if ( !isset( $events['fail_payment'] ) ) {
			$events['fail_payment'] = esc_html__( 'Payment Cancelled', $this->_slug );
		}

		return $events;
	}

}
